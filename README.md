# Yunohost Whitelabel Ready Theme

> Yunohost theme that you can easily modify to customize the look of Yunohost. Remove all Yunohost branding, and replace it with your company logos and names.

<!---
[![Insert Picture Overview Of Theme Here](http://i.imgur.com/)]()
-->

---

## Table of Contents

- [Installation](#installation)
- [How To Customize](#customize)
- [Issues](#issues)
- [Screenshots](#screenshots)

---

## Installation

> For more information, please read <a href="https://yunohost.org/#/theming" target="_blank">here</a>.

### How To

- Clone this repo to your local machine using git.
```bash
git clone https://gitlab.com/facinorous/yunohost-whitelabel.git /usr/share/ssowat/portal/assets/themes/yh-whitelabel
```

- Edit neccessary contents following the [customize instructions below](#customize)

- Edit the ***conf.json.persistent*** file to enable the theme.
```bash
sudo nano /etc/ssowat/conf.json.persistent
```

- Add this to the file. If the theme line already exists for you then change the theme name to ***yh-whitelabel***
```bash
{
    "theme": "yh-whitelabel",
    ...other lines...
}
```

> Go to your website, and hit *ctrl+f5* to refresh the cache and reload the page to see the new theme, or any changes you have made in the file.

---

## Customize

TBA

---

## Issues

TBA
 
---

## Screenshots

TBA

---